import React from 'react';
import { AppContainer } from 'react-hot-loader';

import './styles.scss';

import Gallery from './gallery/Gallery';

const Root = () => (
  <Gallery />
);

export default Root;