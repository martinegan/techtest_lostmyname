import React from 'react';
export default function Image({image}) {
  return (
    <div className="gallery-image">
      <div>
        {image ? <img src={image} /> : null}
      </div>
    </div>
  )
};