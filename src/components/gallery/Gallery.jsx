import React from 'react';
import axios from 'axios';

import Image from './Image';

export default class Gallery extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      images: [],
      selectedImage: '',
    }
  }

  loadImages() {
    axios('http://www.splashbase.co/api/v1/images/latest?images_only')
      .then((response) => {
        this.setState({
          images: response.data.images,
          selectedImage: response.data.images[0],
        });
    });
  }

  componentWillMount() {
    this.loadImages();
  }

  selectImage(image) {
    this.setState({
      selectedImage: image,
    });
  }

  renderImageScroller() {
    return (
      <div className="image-thumbnails">
        {this.state.images.map((image, index) => (
          <div key={index} onClick={this.selectImage.bind(this, image)}>
            <Image image={image.url} />
          </div>
        ))}
      </div> 
    );
  }
  
  render() {
    return (
      <div className="image-gallery" hidden={!this.state.selectedImage}>
        <Image image={this.state.selectedImage.url} />
        {this.renderImageScroller()}
      </div>
    );
  }
};