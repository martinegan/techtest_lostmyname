# LostMy.name - Technical Test #

### How do I get set up? ###
1. Clone repository
1. Run ```yarn install```
2. Run ```yarn start```

### What I would do if I had more time ###
1. Introduce Redux for state management.
2. Implement better navigation between images using forward/backward keys etc
3. Use better styling and CSS to make it look more polished.
4. Add test coverage